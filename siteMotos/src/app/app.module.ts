import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ComponentsComponent } from './components/components.component';
import { CadastroUsuarioComponent } from './components/cadastro-usuario/cadastro-usuario.component';
import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { LoginComponent } from './components/login/login.component';
import { TouringComponent } from './components/touring/touring.component';
import { SportComponent } from './components/sport/sport.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { TrailComponent } from './components/trail/trail.component';
import { CustomComponent } from './components/custom/custom.component';
import { StreetComponent } from './components/street/street.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ComponentsComponent,
    CadastroUsuarioComponent,
    CadastroClientesComponent,
    CadastroMotosComponent,
    LoginComponent,
    TouringComponent,
    SportComponent,
    ScooterComponent,
    TrailComponent,
    CustomComponent,
    StreetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
